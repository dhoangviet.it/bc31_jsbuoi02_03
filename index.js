/*Bai tap 1 */

function tinhLuong() {
  var nhanLuong1Ngay = document.getElementById("txt-nhan-luong-1ngay").value;
  var soNgay = document.getElementById("txt-so-ngay").value;
  var inputKetqua = new Intl.NumberFormat().format(
    Math.floor(soNgay * nhanLuong1Ngay)
  );
  document.getElementById(
    "txtSalary"
  ).innerHTML = `<div>${inputKetqua} VNĐ</div>`;
}
/*Bai tap 2 */

function tinhTB() {
  var num1 = document.getElementById("num1").value * 1;
  var num2 = document.getElementById("num2").value * 1;
  var num3 = document.getElementById("num3").value * 1;
  var num4 = document.getElementById("num4").value * 1;
  var num5 = document.getElementById("num5").value * 1;

  sum = (num1 + num2 + num3 + num4 + num5) / 5;

  document.getElementById("txtTB").innerHTML = `<div>${sum}</div>`;
}

/*Bai tap 3 */

function currency() {
  var usd = document.getElementById("txt-usd").value * 1;
  var result = Intl.NumberFormat().format(usd * 23500);
  document.getElementById("txtCurrency").innerHTML = `<div>${result} VND</div>`;
}
/*Bai Tap 4 */
function tinhSP() {
  var edge1 = document.getElementById("txt-dai").value * 1;
  var edge2 = document.getElementById("txt-rong").value * 1;
  var S = Math.floor(edge1 * edge2);

  var P = Math.floor((edge1 + edge2) * 2);
  document.getElementById(
    "txtCal"
  ).innerHTML = `<div>Diện tích: ${S} -- Chu vi: ${P}</div>`;
}

/*Bai Tap 5 */

function tinhTong() {
  var number = document.getElementById("number").value * 1;
  unit = number % 10;
  ten = Math.floor(number / 10);

  sum = unit + ten;
  document.getElementById("txtSum").innerHTML = `<div>${sum}</div>`;
}
